# 起動方法

!>  文書を読む前に**[Express.js](http://expressjs.com/)**、**[React.js](https://ko.reactjs.org/docs/getting-started.html)**の学習が必要です。

サーバ起動の前に下のツールの設置が必要になります。

```text
node.js
```

?>  node.jsは **[https://nodejs.org/](https://nodejs.org/)** でダウンロードできます。

全てのインストールが終わったら次のようにExpressサーバとReact.jsサーバを起動を行います。

```bash
cd ./node_pro/verify_back
npm start
```

```bash
cd ./node_pro/verify_flont
npm start

npm test
npm run build
```
